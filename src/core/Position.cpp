#include "Position.h"

int Position::getPosX() const
{
    return posX;
}

int Position::getPosY() const
{
    return posY;
}

void Position::setPosX(int x)
{
    posX = x;
}

void Position::setPosY(int y)
{
    posY = y;
}

// Surcharge d'opérateur de comparaison
bool Position::operator == (const Position & p) const {

    return getPosX() == p.getPosX() && getPosY() == p.getPosY();

}

// Surcharge d'opérateur de comparaison
bool Position::operator != (const Position & p) const {

    return getPosX() != p.getPosX() && getPosY() != p.getPosY();
}

