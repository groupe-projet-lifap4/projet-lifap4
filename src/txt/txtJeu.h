#ifndef DEF_MODETXT
#define DEF_MODETXT

#include "../core/Serpent.h"
#include "../core/Position.h"
#include "../core/Fruit.h"
#include "../sfml/Jeu.h"

class modeTxT
{

    private:
        Jeu * j;
        Serpent * s;
        bool vivant;

    public:

        modeTxT();
        ~modeTxT(){};

        bool getVivant() const;
        void initGame(Serpent &s, Fruits &f);

        void setDirectionTxt(Serpent &s);
        void drawTerrain(unsigned int width, unsigned int height);
        void drawSnake();
        void run(Jeu * jeu);

};


#endif // DEF_MODETXT
