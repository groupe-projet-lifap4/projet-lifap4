#ifndef DEF_H_DRAW
#define DEF_H_DRAW

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>

#include "../core/Serpent.h"
#include "Jeu.h"
//#include "Jeu2J.h"
#include "../core/Terrain.h"

/** @brief La classe Draw gère tout ce qui est lié à l'affichage des différents éléments du jeu*/
class Draw
{
    private:
        sf::RenderWindow * app;

        sf::RectangleShape fruit, serpent, scorps, boostPile;

        sf::Clock timer;

        sf::Texture headD, headB, headH, headG;

        sf::Texture bodyD, bodyB, bodyH, bodyG;

        sf::Texture tailD, tailB, tailH, tailG;

        sf::Texture BG, fiul, battery, wallFire;

        sf::Music musicGame, musicLoose;

        sf::SoundBuffer musicEat;

        sf::Sound soundEat;

        sf::Font fontMenu, font2;

        sf::Texture bgMenu, bgScore, bgTest;

        sf::Texture splashScreen;

        sf::RectangleShape wall, step;

        int margin, vitesse, palier;

        std::vector <Position> posWallC;

        std::vector <Position> posStep;
        //sf::Vector2i mousePosition;

        Jeu * j;

//        Jeu2J * j2;

        Serpent * s;

        Terrain * t;

    public:

        /** @brief Afin d'optimiser le code on charge tous les fichiers (sons, musiques, images) dans
le constructeur afin que le chargement ne ce fasse qu'une seul fois */
        Draw(unsigned int Size, unsigned int width, unsigned int height);

        ~Draw();

        /** @brief On lance le jeu */
        void run(Jeu * j);

        Direction setDir(Serpent & s);

        int getMargin() const;

        int getVitesse() const;

        std::vector<Position> & getTabWall();

        void setMargin(int x);

        void setVitesse(int x);

        void menu();

        void startGame(difficuly mode);

        void startGame2J();

        /** @brief On reinitialise les paramètres du jeu pour chaque nouvelle partie */
        void resetGame(Serpent & s, Fruits & f, Terrain &te);

        /** @brief Ici on met en place les fonctionnalités disponible celon les états de jeu.
L'états de jeu est un type Enum que l'on modifie avec un setter */
        gameState etatJeu(int & compteur);

        /** @brief Vérifie que la souris est sur un texte */
        bool mouseOnText(sf::Text t, sf::Vector2i mousePosition);

        /** @brief Effet lorsque la souris est sur un texte */
        void drawHover(sf::Text texte);

        void menuAction();

        void setPosWallC(int tabStart, Position posD, fireWall sW);

        void setPosStep(int tabStart, Position posStart, Step sS);

        void pauseGame(difficuly mode);

        /** @brief On dessine le terrain avec son fond & ses bordures */
        void drawTerrain();


        void drawLoot(Fruits & f, Terrain & te);

        /** @brief L'image de la tête du serpent s'adapte aux déplacements */
        void drawSensHead(Serpent & s);

        /** @brief L'image de la queue du serpent s'adapte aux déplacements */
        void drawSensTail(Serpent  & s);

        /** @brief L'image de la corps du serpent s'adapte aux déplacements */
        void drawSensBody(Serpent & s);

        /** @brief On récupere le score que l'on convertis en string pour l'afficher */
        void drawScore();

        void drawLevelMenu();

        void drawScoreMenu();


        /** @brief On utilise les mêmes méthodes que plus haut pour l'affichage/design/etc... */
        void drawRulesMenu();

        void drawGameOver();

        /** @brief On se sert du tableau de position de la class Terrain pour dessiner les obstacles
sur la map */
        void drawCoinWall(Terrain & t);

        /** @brief On se sert du tableau de position de la class Terrain pour dessiner les obstacles
sur la map */
        void drawStep(Terrain & t);
};

#endif // DER_DRAW
