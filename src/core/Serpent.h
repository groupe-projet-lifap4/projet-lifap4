#ifndef DEF_SERPENT
#define DEF_SERPENT

#include <vector>

#include "Position.h"
#include "Fruit.h"
#include "Terrain.h"

enum Direction {
    HAUT, BAS, GAUCHE, DROITE
};

class Serpent
{
    private:
        std::vector<Position> posSnake;
        std::vector<Position> posTailajouter;
        Direction direction;
//        bool vivant;

    public:
        Serpent();
        ~Serpent(){};

        Direction getDirection();
        std::vector<Position> & getTab(); // accès au tableau de posSnake
        void setTab(int taille, int posX, int posY);

        void suivreTete();
        void posSuivante();
        void setDirection(Direction direction);
        void traverseMur(unsigned int Size);
        bool wallDead(unsigned int Size) const;
        bool mangePomme(Terrain & t, Fruits & f, unsigned int Size);
        void ajouterTailSerpent();
        bool deathColision(std::vector <Position> tab) const;

};

#endif

