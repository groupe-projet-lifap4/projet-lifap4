#include <iostream>
#include <vector>

#include "Terrain.h"
#include "Position.h"
#include "../sfml/Draw.h"

using namespace std;

Terrain::Terrain()
{
    posWall = vector<Position>(48);
    posStep = vector<Position>(64);
}

vector <Position> & Terrain::getTabWall()
{
    return posWall;
}

vector <Position> & Terrain::getTabStep()
{
    return posStep;
}

/* Avec les deux procédures qui suivent on attribut des positions aux obstacles que l'on va
dessiner sur les maps du jeu*/
void Terrain::setTabWall(int tabStart, Position posStart, fireWall sW)
{
    posWall[tabStart] = posStart;

    switch(sW)
    {
        case corner1:
            for(unsigned int i = tabStart+1 ; i <= posWall.size()/8 ; i++)
            {
                posWall[i].setPosX(posWall[i-1].getPosX() + 1);

                posWall[i].setPosY(posWall[tabStart].getPosY());
            }

            for(unsigned int i = (posWall.size()/8)+1 ; i <= (posWall.size()/4)+1 ; i++)
            {
                posWall[i].setPosX(posWall[tabStart].getPosX());

                posWall[i].setPosY(posWall[i-1].getPosY() + 1);
            }
            break;

        case corner2:
            for(unsigned int i = tabStart+1 ; i <= posWall.size()-30 ; i++)
            {
                posWall[i].setPosX(posWall[i-1].getPosX() - 1);

                posWall[i].setPosY(posWall[tabStart].getPosY());
            }

            for(unsigned int i = (posWall.size()-30)+1 ; i <= posWall.size()-24 ; i++)
            {
                posWall[i].setPosX(posWall[tabStart].getPosX());

                posWall[i].setPosY(posWall[i-1].getPosY() + 1);
            }
            break;

        case corner3:
            for(unsigned int i = tabStart+1 ; i <= posWall.size()-18 ; i++)
            {
                posWall[i].setPosX(posWall[i-1].getPosX() + 1);

                posWall[i].setPosY(posWall[tabStart].getPosY());
            }

            for(unsigned int i = (posWall.size()-18)+1 ; i <= posWall.size()-12 ; i++)
            {
                posWall[i].setPosX(posWall[tabStart].getPosX());

                posWall[i].setPosY(posWall[i-1].getPosY() - 1);
            }
            break;

        case corner4:
            for(unsigned int i = tabStart+1 ; i <= posWall.size()-6 ; i++)
            {
                posWall[i].setPosX(posWall[i-1].getPosX() - 1);

                posWall[i].setPosY(posWall[tabStart].getPosY());
            }

            for(unsigned int i = (posWall.size()-6)+1 ; i <= posWall.size() ; i++)
            {
                posWall[i].setPosX(posWall[tabStart].getPosX());

                posWall[i].setPosY(posWall[i-1].getPosY() - 1);
            }
            break;

        default:
            break;
    }
}

void Terrain::setTabStep(int tabStart, Position posStart, Step sS)
{
    posStep[tabStart] = posStart;
    //Position posTempo;

    switch(sS)
    {
        case hG:
            for(unsigned int i = tabStart+1 ; i < 16 ; i++)
            {
                if(i%2 == 0) {
                    posStep[i].setPosX(posStep[i-1].getPosX());
                    posStep[i].setPosY(posStep[i-1].getPosY() + 1);
                }

                else {
                    posStep[i].setPosX(posStep[i-1].getPosX() + 1);
                    posStep[i].setPosY(posStep[i-1].getPosY());
                }
            }
            break;

        case hD:
            for(unsigned int i = tabStart+1 ; i < 32 ; i++)
            {
                if(i%2 == 0) {
                    posStep[i].setPosX(posStep[i-1].getPosX());
                    posStep[i].setPosY(posStep[i-1].getPosY() + 1);
                }

                else {
                    posStep[i].setPosX(posStep[i-1].getPosX() - 1);
                    posStep[i].setPosY(posStep[i-1].getPosY());
                }
            }
            break;

        case bG:
            for(unsigned int i = tabStart+1 ; i < 48 ; i++)
            {
                if(i%2 == 0) {
                    posStep[i].setPosX(posStep[i-1].getPosX());
                    posStep[i].setPosY(posStep[i-1].getPosY() - 1);
                }

                else {
                    posStep[i].setPosX(posStep[i-1].getPosX() + 1);
                    posStep[i].setPosY(posStep[i-1].getPosY());
                }
            }
            break;

        case bD:
            for(unsigned int i = tabStart+1 ; i < 64 ; i++)
            {
                if(i%2 == 0) {
                    posStep[i].setPosX(posStep[i-1].getPosX());
                    posStep[i].setPosY(posStep[i-1].getPosY() - 1);
                }

                else {
                    posStep[i].setPosX(posStep[i-1].getPosX() - 1);
                    posStep[i].setPosY(posStep[i-1].getPosY());
                }
            }
            break;
    }
}
