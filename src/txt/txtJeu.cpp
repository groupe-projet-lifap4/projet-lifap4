#include "txtJeu.h"

#include <iostream>
//#include <conio.h>
#include <vector>

using namespace std;

modeTxT::modeTxT()
{
    vivant = true;
}

bool modeTxT::getVivant() const
{
    return vivant;
}

void modeTxT::setDirectionTxt(Serpent &s)
{
    //Direction d = s.getDirection();
    int touch = _getch();
    if(kbhit())
    {
        switch(touch)
        {
        case('z'):
            s.setDirection(HAUT);
            break;
        case('s'):
            s.setDirection(BAS);
            break;
        case('q'):
            s.setDirection(GAUCHE);
            break;
        case('d'):
            s.setDirection(DROITE);
            break;
        }
    }
}

void modeTxT::drawSnake()
{
    Serpent &s = j->getSerpent();
    s.setTab(3, j->getWidth()/2, j->getHeight()/2);

    vector <Position> & T = s.getTab();
    for(unsigned int i = 0 ; i < T.size() ; i++)
    {
        if(i == 0) cout<<"@";
        else {
            cout<<"s";
        }
    }
}

void modeTxT::drawTerrain(unsigned int width, unsigned int height)
{
    Serpent &s = j->getSerpent();
    vector <Position> & T = s.getTab();

    system("cls");
    for(unsigned int i = 0 ; i <= width ; i++)
        cout<<"#";

    cout<<endl;

    for(unsigned int i=0 ; i < height ; i++)
    {
        for(unsigned int j=0 ; j < width ; j++)
        {
            if(j == 0)
                cout<<"#";

            if (i == T[0].getPosY() && j == T[0].getPosX())
                cout << "s";

            /*else if (i == fruitY && j == fruitX)
                cout << "F";*/

            else {
                 cout<<" ";
            }

            if(j == width - 1) cout<<"#";
        }
        cout<<endl;
    }

    for(unsigned int i = 0 ; i <= width ; i++)
        cout<<"#";

    cout<<endl;
}

void modeTxT::run(Jeu * jeu)
{
    j = jeu;

    while(vivant)
    {
        drawTerrain(j->getSize(), j->getSize());
        drawSnake();
        setDirectionTxt(j->getSerpent());
    }

}
