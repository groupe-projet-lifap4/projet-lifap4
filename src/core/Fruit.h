#ifndef DEF_FRUITS
#define DEF_FRUITS

#include <vector>

#include "Position.h"

/** @brief Un fruit est défini par une position et est l'objet permettant d'augmenter le score (10pts à chaque fruit mangé) */
class Fruits
{
    private:
        Position pos;
        int point;

    public:

        Fruits();
        ~Fruits(){};

        /** @brief Getter pour récuperer la posX du fruit */
        int getFruitX() const;

        /** @brief Getter pour récuperer la posY du fruit */
        int getFruitY() const;

        /** @brief Fonction booléen qui prend un vector en paramètre &
Return true si la Pos du fruit est dans le tableau de vector */
        bool posTest(std::vector <Position> tab) const;

        /** @brief Attribut une position random sur la map au fruit */
        void posRand(int Size);

};

#endif
