#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "../core/Serpent.h"
#include "../core/Fruit.h"
#include "Jeu.h"
#include "Draw.h"
#include <cstdlib>


using namespace std;
using namespace sf;


int main()
{
    srand(time(NULL));

    Jeu j;
    Draw d(j.getSize(), j.getWidthWindow(), j.getHeightWindow());

    d.run(&j);

    return 0;
}

