EXEC_NAME = ./bin/Debug/AANSnake
OBJ_CORE = ./obj/Debug/src/core/*.o
OBJ_SFML = ./obj/Debug/src/sfml/*.o
HEADERS_CORE = ./src/core/*.h

CC = g++
CFLAGS = -Wall -ggdb
INCLUDES = -I./extern/SFML-2.5.1/include/SFML
LIBS = -L./extern/SFML-2.5.1/lib -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

all: $(EXEC_NAME)

$(EXEC_NAME): $(OBJ_SFML) $(OBJ_CORE)
	$(CC) -o $(EXEC_NAME) $(OBJ_SFML) $(OBJ_CORE) $(INCLUDES) $(LIBS)
	
./obj/Debug/src/sfml/main_sfml.o: ./src/sfml/main_sfml.cpp $(HEADERS_CORE)
	$(CC) $(CFLAGS) $(INCLUDES) -c ./src/sfml/main_sfml.cpp
	
./obj/Debug/src/core/Terrain.o: ./src/core/Terrain.cpp ./src/core/Terrain.h
	$(CC) $(CFLAGS) $(INCLUDES) -c ./src/core/Terrain.cpp
	
./obj/Debug/src/core/Position.o: ./src/core/Position.cpp ./src/core/Position.h
	$(CC) $(CFLAGS) $(INCLUDES) -c ./src/core/Position.cpp
	
./obj/Debug/src/core/Serpent.o: ./src/core/Serpent.cpp ./src/core/Serpent.h ./src/core/Position.h
	$(CC) $(CFLAGS) $(INCLUDES) -c ./src/core/Serpent.cpp
	
clean: 
	rm$(OBJ_FILES)
