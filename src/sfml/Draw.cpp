#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>


#include "Draw.h"
#include "../core/Terrain.h"


using namespace std;
using namespace sf;


Draw::Draw(unsigned int Size, unsigned int widthWindow, unsigned int heightWindow)
{
    app = new  RenderWindow(VideoMode(widthWindow + 100, heightWindow + 100), "Snake Game");
    app->setFramerateLimit(60);

    if (!headD.loadFromFile("../../data/textures/head.png")) cout<<"Erreur de chargement."<<endl;
    if (!headB.loadFromFile("../../data/textures/headB.png")) cout<<"Erreur de chargement."<<endl;
    if (!headH.loadFromFile("../../data/textures/headH.png")) cout<<"Erreur de chargement."<<endl;
    if (!headG.loadFromFile("../../data/textures/headG.png")) cout<<"Erreur de chargement."<<endl;

    if (!bodyD.loadFromFile("../../data/textures/body.png")) cout<<"Erreur de chargement."<<endl;
    if (!bodyB.loadFromFile("../../data/textures/bodyB.png")) cout<<"Erreur de chargement."<<endl;
    if (!bodyH.loadFromFile("../../data/textures/bodyH.png")) cout<<"Erreur de chargement."<<endl;
    if (!bodyG.loadFromFile("../../data/textures/bodyG.png")) cout<<"Erreur de chargement."<<endl;

    if (!tailD.loadFromFile("../../data/textures/queu.png")) cout<<"Erreur de chargement."<<endl;
    if (!tailB.loadFromFile("../../data/textures/queuB.png")) cout<<"Erreur de chargement."<<endl;
    if (!tailH.loadFromFile("../../data/textures/queuH.png")) cout<<"Erreur de chargement."<<endl;
    if (!tailG.loadFromFile("../../data/textures/queuG.png")) cout<<"Erreur de chargement."<<endl;

    if (!BG.loadFromFile("../../data/textures/bg2.png")) cout<<"Erreur de chargement."<<endl;

    if (!bgTest.loadFromFile("../../data/textures/menu.jpg")) cout<<"Erreur de chargement."<<endl;

    if (!fiul.loadFromFile("../../data/textures/fiul2.png")) cout<<"Erreur de chargement."<<endl;

    if (!battery.loadFromFile("../../data/textures/battery.png")) cout<<"Erreur de chargement."<<endl;

    if (!wallFire.loadFromFile("../../data/textures/fml.jpg")) cout<<"Erreur de chargement."<<endl;

    if (!musicEat.loadFromFile("../../data/musiques/manger.wav")) cout<<"erreur lors de la lecture du fichier audio.";
        soundEat.setBuffer(musicEat);

    if (!musicGame.openFromFile("../../data/musiques/bigBurger.wav")) cout<<"Erreur de chargement."<<endl;

    if (!musicLoose.openFromFile("../../data/textures/nikomok.wav")) cout<<"Erreur de chargement."<<endl;

    if (!fontMenu.loadFromFile("../../data/fonts/sho.ttf")) cout<<"Erreur de chargement."<<endl;

    if (!font2.loadFromFile("../../data/fonts/lethal.ttf")) cout<<"Erreur de chargement."<<endl;

    if (!bgMenu.loadFromFile("../../data/textures/bgMenu.jpg")) cout<<"Erreur de chargement."<<endl;

    if(!bgScore.loadFromFile("../../data/textures/bgScore2.jpg")) cout<<"Erreur de chargement"<<endl;

    if(!splashScreen.loadFromFile("../../data/textures/BGsnake.jpg")) cout<<"Erreur de chargement."<<endl;

    margin = 25;
    palier = 50;
    vitesse = 90;

    wall = RectangleShape(Vector2f(28, 28));
    fruit = RectangleShape(Vector2f(28, 28));
    boostPile = RectangleShape(Vector2f(28,28));
    serpent = RectangleShape(Vector2f(28, 28));
    scorps = RectangleShape(Vector2f(28, 28));
    step = RectangleShape(Vector2f(28,28));

    //wall.setFillColor(Color::Yellow);
    //wall.setOutlineThickness(2);
    fruit.setTexture(&fiul);
    boostPile.setTexture(&battery);
    wall.setTexture(&wallFire);
    step.setTexture(&wallFire);
    //mousePosition = Mouse::getPosition(*app);
}


Draw::~Draw()
{
    delete app;
}

int Draw::getMargin() const
{
    return margin;
}

void Draw::setMargin(int x)
{
    margin = x;
}

int Draw::getVitesse() const
{
    return vitesse;
}

void Draw::setVitesse(int x)
{
    vitesse = x;
}

Direction Draw::setDir(Serpent & s)
{
        Direction d = s.getDirection();
        if(Keyboard::isKeyPressed(Keyboard::Up) && d != BAS)
        {
            s.setDirection(HAUT);
        }
        else if(Keyboard::isKeyPressed(Keyboard::Down) && d != HAUT)
        {
            s.setDirection(BAS);
        }
        else if(Keyboard::isKeyPressed(Keyboard::Right) && d != GAUCHE)
        {
            s.setDirection(DROITE);
        }
        else if(Keyboard::isKeyPressed(Keyboard::Left) && d != DROITE)
        {
            s.setDirection(GAUCHE);
        }
        return d;
    /*
    if(kbhit())
    {
        switch(frappe)
        {
        case(Keyboard::Up):
            s.setDirection(HAUT);
            break;
        case('B'):
            s.setDirection(BAS);
            break;
        case('D'):
            s.setDirection(GAUCHE);
            break;
        case('C'):
            s.setDirection(DROITE);
            break;
        }
    }
    */
}

vector <Position> & Draw::getTabWall()
{
    return posWallC;
}


void Draw::drawSensHead(Serpent & s)
{
      switch ( setDir(s) )
      {
         case DROITE:
                serpent.setTexture(&headD);
                app->draw(serpent);
                break;
         case BAS:
                serpent.setTexture(&headB);
                app->draw(serpent);
                break;
         case HAUT:
                serpent.setTexture(&headH);
                app->draw(serpent);
                break;
         case GAUCHE:
                serpent.setTexture(&headG);
                app->draw(serpent);
                break;
         default:
            break;
      }
}


void Draw::drawSensBody(Serpent & s)
{
      switch ( setDir(s) )
      {
         case DROITE:
                scorps.setTexture(&bodyD);
                app->draw(scorps);
                break;
         case BAS:
                scorps.setTexture(&bodyB);
                app->draw(scorps);
                break;
         case HAUT:
                scorps.setTexture(&bodyH);
                app->draw(scorps);
                break;
         case GAUCHE:
                scorps.setTexture(&bodyG);
                app->draw(scorps);
                break;
         default:
            break;
      }
}


void Draw::drawSensTail(Serpent & s)
{
      switch ( setDir(s) )
      {
         case DROITE:
                scorps.setTexture(&tailD);
                app->draw(scorps);
                break;
         case BAS:
                scorps.setTexture(&tailB);
                app->draw(scorps);
                break;
         case HAUT:
                scorps.setTexture(&tailH);
                app->draw(scorps);
                break;
         case GAUCHE:
                scorps.setTexture(&tailG);
                app->draw(scorps);
                break;
         default:
            break;
      }

}


void Draw::drawTerrain()
{
    RectangleShape terrain(Vector2f(j->getWidth(), j->getHeight()));

    terrain.setTexture(&bgMenu);
    terrain.setPosition(margin, margin);
    terrain.setOutlineColor(Color::White);
    terrain.setOutlineThickness(2);

    app->draw(terrain);
}

void Draw::drawLoot(Fruits & f, Terrain & t)
{
    //Serpent & s = j->getSerpent();
    if(j->getScore() == palier) {
        boostPile.setPosition(margin + f.getFruitX() * j->getSize(),
                        margin + f.getFruitY()* j->getSize());
        app->draw(boostPile);
        //if(s.mangePomme(t, f, j->getScore())) palier += 20;
    }
    else {
        fruit.setPosition(margin + f.getFruitX() * j->getSize(),
                        margin + f.getFruitY()* j->getSize());
        app->draw(fruit);
    }
}


void Draw::drawScore()
{
    Text text;

        unsigned int rec = j->getScore();
        string str = to_string(rec);

        text.setFont(fontMenu);
        text.setString("SCORE : " + str);
        text.setPosition(900, 30);

        RectangleShape menu(Vector2f(300, 200));

        menu.setTexture(&bgScore);
        menu.setPosition(850, 25);
        menu.setOutlineColor(Color::White);
        menu.setOutlineThickness(1);


        app->draw(menu);
        app->draw(text);
}


void Draw::drawCoinWall(Terrain & t)
{
    Position pos1 = Position(0, 0);
    Position pos2 = Position(28, 0);
    Position pos3 = Position(0, 28);
    Position pos4 = Position(28, 28);

    t.setTabWall(0, pos1, corner1);

    t.setTabWall(12, pos2, corner2);

    t.setTabWall(24, pos3, corner3);

    t.setTabWall(36, pos4, corner4);

    vector <Position> & T = t.getTabWall();

    for(unsigned int i = 0 ; i < T.size() ; i++)
    {
        wall.setPosition(T[i].getPosX() * 28 + margin,
                         T[i].getPosY()* 28 + margin);

        //cout<<T[i].getPosX()<<"  "<<T[i].getPosY()<<endl;
        app->draw(wall);
    }
}


void Draw::drawStep(Terrain & t)
{
    Position pos1 = Position(5,6);
    Position pos2 = Position(24,6);
    Position pos3 = Position(5, 22);
    Position pos4 = Position(24, 22);

    t.setTabStep(0, pos1, hG);
    t.setTabStep(16, pos2, hD);
    t.setTabStep(32, pos3, bG);
    t.setTabStep(48, pos4, bD);

    vector<Position> & T = t.getTabStep();

    for(unsigned int i = 0 ; i < T.size() ; i++)
    {
        step.setPosition(T[i].getPosX() * 28 + margin,
                         T[i].getPosY() * 28 + margin);
        //cout<<posStep[i].getPosX()<<"  "<<posStep[i].getPosY()<<endl;
        app->draw(step);
    }

}

void Draw::startGame(difficuly mode)
{
    Serpent & s = j->getSerpent();
    Fruits & f = j->getFruits();
    Terrain & t = j->getTerrain();

    setDir(s);
    j->update();
    drawTerrain();

    switch(mode) // dans ce switch case le premier cas englobe les autres
    {
        case Hard:
            drawStep(t);
            if(s.deathColision(t.getTabStep()))
                    j->setGameState(Looser);

        case Medium:
            drawCoinWall(t);
            //if(j->augmentationSpeed()) vitesse = 50;
            if(s.deathColision(t.getTabWall())) j->setGameState(Looser);

        case Easy:
            if(s.deathColision(s.getTab())) j->setGameState(Looser);
            s.traverseMur(j->getSize());
            break;
    }

   /* while(f.posTest(posStep) || f.posTest(posWallC) || f.posTest(s.getTab()))
          f.posRand(j->getSize()); */


    drawLoot(f, t);

    vector<Position> & T = s.getTab();

    serpent.setPosition(margin + T[0].getPosX() * j->getSize(),
                                margin + T[0].getPosY() * j->getSize());

    drawSensHead(s);

    for(unsigned int i = 1 ; i < T.size() ; i++) {

        if(i != T.size()-1) {
                scorps.setPosition(margin + T[i].getPosX() * j->getSize(),
                                    margin + T[i].getPosY() * j->getSize());
                drawSensBody(s);
        }
        else {
                scorps.setPosition(margin + T[i].getPosX() * j->getSize(),
                                    margin + T[i].getPosY() * j->getSize());
                drawSensTail(s);
        }
        //cout<<T[0].getPosX()<<"  "<<T[0].getPosY()<<endl;
    }

    drawScore();

    bool sound = j->getSoundEat();
    if(sound != false)
    {
        soundEat.setVolume(80);
        soundEat.play();
    }
}



void Draw::pauseGame(difficuly mode)
{
    Serpent & s = j->getSerpent();
    Fruits & f = j->getFruits();

    setDir(s);
    drawTerrain();

    switch(mode)
    {
        case Hard:
            //drawStep();
            if(s.deathColision(posStep)) j->setGameState(Looser);

        case Medium:
            //drawCoinWall();
            if(s.wallDead(j->getSize())) j->setGameState(Looser);

        case Easy:
            if(s.deathColision(s.getTab())) j->setGameState(Looser);
            s.traverseMur(j->getSize());
            break;
    }

    fruit.setPosition(margin + f.getFruitX() * j->getSize(),
                            margin + f.getFruitY()* j->getSize());

    app->draw(fruit);

    vector<Position> & T = s.getTab();

    serpent.setPosition(margin + T[0].getPosX() * j->getSize(),
                                margin + T[0].getPosY() * j->getSize());

    drawSensHead(s);

    for(unsigned int i = 1 ; i < T.size() ; i++) {

        if(i != T.size()-1) {
                scorps.setPosition(margin + T[i].getPosX() * j->getSize(),
                                    margin + T[i].getPosY() * j->getSize());
                drawSensBody(s);
        }
        else {
                scorps.setPosition(margin + T[i].getPosX() * j->getSize(),
                                    margin + T[i].getPosY() * j->getSize());
                drawSensTail(s);
        }
        //cout<<T[0].getPosX()<<"  "<<T[0].getPosY()<<endl;
    }

    drawScore();

    bool sound = j->getSoundEat();
    if(sound != false)
    {
        soundEat.setVolume(80);
//        soundEat.play();
    }
}


void Draw::resetGame(Serpent &s, Fruits &f, Terrain &te)
{
    s.setTab(3, 2, 14);
    j->setScore(0);
    vitesse = 90;
    s.setDirection(DROITE);
    f.posRand(j->getSize());
    while(f.posTest(te.getTabStep()) || f.posTest(te.getTabWall()) || f.posTest(s.getTab()))
          f.posRand(j->getSize());
    j->setGameState(Play);
}


bool Draw::mouseOnText(Text texte, Vector2i  mousePosition)
{
    FloatRect rect;
    //bool contain = rect.contains(mousePosition.x, mousePosition.y);

    rect = texte.getGlobalBounds();

    if(rect.contains(mousePosition.x, mousePosition.y)) return true;

    return false;
}


void Draw::drawHover(Text texte)
{
    texte.setOutlineColor(Color::Blue);
    texte.setOutlineThickness(3);
    app->draw(texte);
}


void Draw::menu()
{
    app->clear();

    //musicGame.setVolume(50);

    RectangleShape windowMenu(Vector2f(j->getWidthWindow(), j->getHeightWindow()));

    Vector2i mousePosition = Mouse::getPosition(*app);

    windowMenu.setTexture(&bgMenu);
    windowMenu.setPosition(50, 50);
    windowMenu.setOutlineColor(Color::White);
    windowMenu.setOutlineThickness(2);

    Text play("PLAY", fontMenu, 60);
        play.setPosition(450, 250);

    Text highScore("HIGH SCORE", fontMenu, 60);
        highScore.setPosition(450, 350);

    Text rules("RULES", fontMenu, 60);
        rules.setPosition(450, 450);

    Text exit("EXIT", fontMenu, 60);
        exit.setPosition(450, 550);

        app->draw(windowMenu);
        app->draw(play);
        app->draw(highScore);
        app->draw(rules);
        app->draw(exit);

        bool clic = Mouse::isButtonPressed(Mouse::Left);
        // Si la souris est sur le texte on applique l'effet
        //Si on est sur le texte & que l'on clic gauche on arrive sur une autre page
        if(mouseOnText(play, mousePosition)) {

            if(clic) {
                j->setGameState(MenuLevel);
                return;
            }
            drawHover(play);
        }
        if(mouseOnText(highScore, mousePosition)) {

            if(clic) {
                j->setGameState(MenuScore);
                return;
            }
            drawHover(highScore);
        }
        if(mouseOnText(rules, mousePosition)) {

            if(clic) {
                j->setGameState(MenuRules);
                return;
            }
            drawHover(rules);
        }
        if(mouseOnText(exit, mousePosition)) {

                if(clic) {
                    app->close();
                }
            drawHover(exit);
        }
}


void Draw::drawLevelMenu()
{
    Serpent & s = j->getSerpent();
    Fruits & f = j->getFruits();
    Terrain & t = j->getTerrain();

    RectangleShape windowLevelMenu(Vector2f(j->getWidthWindow(), j->getHeightWindow()));

    Vector2i mousePosition = Mouse::getPosition(*app);

    bool clic = Mouse::isButtonPressed(Mouse::Left);

    windowLevelMenu.setTexture(&bgMenu);
    windowLevelMenu.setPosition(50, 50);
    windowLevelMenu.setOutlineColor(Color::White);
    windowLevelMenu.setOutlineThickness(2);

    Text easy("EASY", fontMenu, 60);
        easy.setPosition(500, 200);

    Text medium("MEDIUM", fontMenu, 60);
        medium.setPosition(500, 350);

    Text hard("HARD", fontMenu, 60);
        hard.setPosition(500, 500);

    Text deuxJoueurs("JOUER À 2", fontMenu, 60);
        deuxJoueurs.setPosition(500, 650);

    Text bacck("BACK", fontMenu, 60);
        bacck.setPosition(100, 100);

        app->draw(windowLevelMenu);
        app->draw(bacck);
        app->draw(easy);
        app->draw(medium);
        app->draw(hard);
        app->draw(deuxJoueurs);


    if(mouseOnText(easy, mousePosition)) {

            if(clic) {
                resetGame(s, f, t);
                j->setDifficulty(Easy);
                j->setGameState(Play);

                return;
            }
            drawHover(easy);
        }
    if(mouseOnText(medium, mousePosition)) {

            if(clic) {
                resetGame(s, f, t);
                j->setDifficulty(Medium);
                j->setGameState(Play);

                return;
            }
            drawHover(medium);
        }
    if(mouseOnText(hard, mousePosition)) {

            if(clic) {
                resetGame(s, f, t);
                j->setDifficulty(Hard);
                j->setGameState(Play);

                return;
            }
            drawHover(hard);
        }
    if(mouseOnText(bacck, mousePosition)) {

            if(clic) {
                j->setGameState(Menu);
            }
            drawHover(bacck);
        }
    /*if(mouseOnText(deuxJoueurs, mousePosition)) {

            if(clic) {
                j->setDifficulty(twoPlayers);
                j->setGameState(Play);
            }
            drawHover(deuxJoueurs);
        }*/
}

void Draw::drawScoreMenu()
{
    RectangleShape windowScoreMenu(Vector2f(j->getWidthWindow(), j->getHeightWindow()));

    Vector2i mousePosition = Mouse::getPosition(*app);

    bool clic = Mouse::isButtonPressed(Mouse::Left);

    windowScoreMenu.setTexture(&bgMenu);
    windowScoreMenu.setPosition(50, 50);
    windowScoreMenu.setOutlineColor(Color::White);
    windowScoreMenu.setOutlineThickness(2);

    //On récupère les scores dans les fichiers
    string strE, strM, strH;
    strE = j->readInFile("../../data/fichierTxt/easyScore.txt");
    strM = j->readInFile("../../data/fichierTxt/mediumScore.txt");
    strH = j->readInFile("../../data/fichierTxt/hardScore.txt");

    Text bacck("BACK", fontMenu, 60);
        bacck.setPosition(100, 100);

    //On applique un design aux scores
    Text highScoreE("BEST SCORE LEVEL EASY : " + strE, fontMenu, 60);
        highScoreE.setPosition(100, 300);

    Text highScoreM("BEST SCORE LEVEL MEDIUM : " + strM, fontMenu, 60);
        highScoreM.setPosition(100, 400);

    Text highScoreH("BEST SCORE LEVEL HARD : " + strH, fontMenu, 60);
        highScoreH.setPosition(100, 500);

    //on dessine toutes les éléments
    app->draw(windowScoreMenu);
    app->draw(bacck);
    app->draw(highScoreE);
    app->draw(highScoreM);
    app->draw(highScoreH);

    if(mouseOnText(bacck, mousePosition)) {

            if(clic) {
                j->setGameState(Menu);
            }
            drawHover(bacck);
    }
}

void Draw::drawRulesMenu()
{
    RectangleShape windowScoreMenu(Vector2f(j->getWidthWindow(), j->getHeightWindow()));

    Vector2i mousePosition = Mouse::getPosition(*app);

    bool clic = Mouse::isButtonPressed(Mouse::Left);

    windowScoreMenu.setTexture(&bgMenu);
    windowScoreMenu.setPosition(50, 50);
    windowScoreMenu.setOutlineColor(Color::White);
    windowScoreMenu.setOutlineThickness(2);

    //string rules = readInFile("rules.txt");
    string rulesTxt =
    "IN EACH LEVEL U SHOULD EAT THE MORE LOOT THAT U CAN.\n\n EASY MODE: JUST DONT EAT OURSELF IF U DONT WANT \n TO DIE. \n\n MEDIUM MODE: U CAN NOT CROSS ANYMORE THE WALL. \n\n GAME MODE: FLAMES APPEARED IN ADDITION OF THE \n OTHERS DIFFICULTY.";

    Text bacck("BACK", fontMenu, 60);
        bacck.setPosition(100, 100);

    Text rules(rulesTxt, fontMenu, 35);
        rules.setPosition(90, 250);

    app->draw(windowScoreMenu);
    app->draw(bacck);
    app->draw(rules);


    if(mouseOnText(bacck, mousePosition)) {

            if(clic) {
                j->setGameState(Menu);
            }
            drawHover(bacck);
    }
}

void Draw::drawGameOver()
{
    RectangleShape windowGameOver(Vector2f(j->getWidthWindow(), j->getHeightWindow()));

    Vector2i mousePosition = Mouse::getPosition(*app);

    bool clic = Mouse::isButtonPressed(Mouse::Left);
    bool enter = Keyboard::isKeyPressed(Keyboard::Return);

    windowGameOver.setTexture(&bgMenu);
    windowGameOver.setPosition(50, 50);
    windowGameOver.setOutlineColor(Color::White);
    windowGameOver.setOutlineThickness(2);

    Text looser("GAME OVER !", fontMenu, 80);
        looser.setOutlineColor(Color::Red);
        looser.setOutlineThickness(3);
        looser.setPosition(350, 180);

    Text again("PRESS ENTER FOR PLAY AGAIN", fontMenu, 60);
        again.setPosition(150, 450);

    Text menu("MENU", fontMenu, 60);
        menu.setPosition(530, 600);

        app->draw(windowGameOver);
        app->draw(looser);
        app->draw(again);
        app->draw(menu);

    if(enter) j->setGameState(PlayAgain);

    if(mouseOnText(again, mousePosition)) {

            if(clic) {
                j->setGameState(PlayAgain);
            }
            drawHover(again);
        }
    if(mouseOnText(menu, mousePosition)) {

        if(clic) {
            j->setGameState(Menu);
        }
        drawHover(menu);
    }
}


gameState Draw::etatJeu(int & compteur)
{
    gameState etat = j->getGameState();
    Serpent & s = j->getSerpent();
    Fruits & f = j->getFruits();
    Terrain & t = j->getTerrain();

    switch(etat)
    {
        case Menu: {
            compteur++;
            RectangleShape window(Vector2f(700, 400)); //Img: 666x378
            window.setTexture(&splashScreen);
            window.setOutlineColor(Color::White);
            window.setOutlineThickness(3);
            window.setPosition(250, 200);
            app->draw(window);

            if (compteur > 50) menu();
            break;
        }

        case MenuLevel:
            drawLevelMenu();
            break;

        case MenuScore:
            drawScoreMenu();
            break;

        case MenuRules:
            drawRulesMenu();
            break;

        case Play:
            startGame(j->getDifficulty());
            break;

        /*case Play2J:
            startGame2J();*/

        case Pause:
            cout<<"pause"<<endl;
            break;

        case Looser:
            //musicLoose.play();
            drawGameOver();
            j->writeInFile(j->getScore(), j->getDifficulty());

            break;

        case PlayAgain:
            resetGame(s, f, t);
            break;

        default:
            break;
    }

    return etat;
}


void Draw::run(Jeu * jeu)
{
    j = jeu;

    musicGame.play();
    musicGame.setVolume(50);
    int counter = 0;
    while(app->isOpen()) {

            Event event;
            while(app->pollEvent(event))
            {
                if(event.type == Event::KeyReleased)
                {
                    if(event.key.code == Keyboard::Escape)
                    {
                        app->close();
                        break;
                    }

                    /*if(event.key.code == Keyboard::Space)
                    {
                        j->setGameState(Pause);
                        bool gamePause = true;
                        while(gamePause)
                        {
                            pauseGame(j->getDifficulty());
                            if(event.key.code == Keyboard::Space) gamePause = false;
                        }
                    }
                    timer.restart();*/
                }
            }

            app->clear();

            etatJeu(counter);
            sleep(milliseconds(vitesse));
            app->display();
            timer.restart();
    }
}


