#include <iostream>
#include <stdlib.h>
#include <time.h>

#include "Fruit.h"
#include "Position.h"
#include "Serpent.h"

using namespace std;


Fruits::Fruits()
{
    point = 10;
}


bool Fruits::posTest(vector <Position> tab) const
{
    for(unsigned int i = 0 ; i < tab.size() ; i++)
        if(pos == tab[i])
            return true;

    return false;
}


void Fruits::posRand(int Size)
{
    Serpent s;

        int x = rand() % Size;
        int y = rand() % Size;

        pos.setPosX(x);
        pos.setPosY(y);
}


int Fruits::getFruitX() const {
    return pos.getPosX();
}


int Fruits::getFruitY() const {
    return pos.getPosY();
}
