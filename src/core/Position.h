#ifndef POSITION_H_INCLUDED
#define POSITION_H_INCLUDED

class Position
{
    private:
        int posX, posY;

    public:
        Position(int x = 0, int y = 0) {
            posX = x;
            posY = y;
        }
        int getPosX() const;
        int getPosY() const;

        void setPosX(int);
        void setPosY(int);
        bool operator == (const Position &) const;
        bool operator != (const Position &) const;
};

#endif // POSITION_H_INCLUDED
