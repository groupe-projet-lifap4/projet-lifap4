#include <iostream>
#include <fstream>
#include <string>

#include "Jeu.h"
#include "../core/Serpent.h"
#include "../core/Fruit.h"


using namespace std;


Jeu::Jeu()
{
    score = 0;
    Size = 28;
    width = Size*Size + Size;
    height = Size*Size + Size;
    widthWindow = 1100;
    heightWindow = 775;
    soundEat = false;
    f.posRand(Size);
    gState = Menu;
    mode = Easy;
}


void Jeu::update()
{
    //vector<Position> & T = s.getTab();
    if(s.mangePomme(t, f, Size))
    {
        soundEat = true;
        score += 10;
    }
    s.suivreTete();
    s.ajouterTailSerpent();
    s.posSuivante();
    //cout<<"x: "<<T[0].getPosX()<<" y: "<<T[0].getPosY()<<endl;

}

Serpent & Jeu::getSerpent()
{
    return s;
}

Fruits & Jeu::getFruits()
{
    return f;
}

Terrain & Jeu::getTerrain()
{
    return t;
}

unsigned int Jeu::getSize() const
{
    return Size;
}

unsigned int Jeu::getWidth() const
{
    return width;
}

unsigned int Jeu::getHeight() const
{
    return height;
}

unsigned int Jeu::getWidthWindow() const
{
    return widthWindow;
}

unsigned int Jeu::getHeightWindow() const
{
    return heightWindow;
}

unsigned int Jeu::getScore() const
{
    return score;
}

void Jeu::setScore(unsigned int x)
{
    score = x;
}



bool Jeu::getSoundEat()
{
    bool tmp = soundEat;
    soundEat = false;
    return tmp;
}

gameState Jeu::getGameState() const
{
    return gState;
}

void Jeu::setGameState(gameState state)
{
    gState = state;
}

difficuly Jeu::getDifficulty() const
{
    return mode;
}

void Jeu::setDifficulty(difficuly level)
{
    mode = level;
}


string Jeu::readInFile(string file)
{
     ifstream fichier(file, ios::in);

        if(fichier)
        {
                string score;

                fichier >> score;  /// on lit jusqu'à l'espace et on stocke ce qui est lu dans la variable indiquée

                return score;

                fichier.close();
        }
        else
                cerr << "Impossible d'ouvrir le fichier !" << endl;
}


void Jeu::writeInFile(unsigned int score, difficuly level)
{
    string file, str = to_string(score);
    switch(level)
    {
        case Easy:
            file = "data/fichierTxt/easyScore.txt";
            break;
        case Medium:
            file = "data/fichierTxt/mediumScore.txt";
            break;
        case Hard:
            file = "data/fichierTxt/hardScore.txt";
            break;

        default:
            break;
    }

    string testScore = readInFile(file);
    int intTestScore = stoi(testScore, nullptr, 10); /// convertit str to int avec choix de la base

    if(intTestScore < score) {
        fstream fichier(file, ios::out);

            if(fichier)
            {
                fichier<<str<<" ";
                fichier.close();
            }
            else
                    cerr << "Erreur à l'ouverture !" << endl;
    }
}

bool Jeu::augmentationSpeed()
{
    if(getScore() > 20) return true;

    return false;
}
