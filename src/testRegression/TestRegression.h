#ifndef TESTREGRESSION_H
#define TESTREGRESSION_H

#include <iostream>

#include "../core/Terrain.h"
#include "../core/Serpent.h"
#include "../core/Fruit.h"
#include "../sfml/Jeu.h"
#include "../sfml/Draw.h"
#include "../core/Position.h"

#include <assert.h>

using namespace std;


/**
		@brief la classe TestRegression permet de tester toutes les fonctions implémentées dans le jeu
*/


class TestRegression
{
    public:

        ~TestRegression();

        void testWindow(sf::RenderWindow * app, int error);
        void testShape(sf::RectangleShape * rec, int er);
        void testTexture(sf::Texture * texture, int er);
        void testFont(sf::Font * font, int er);
        void testMusique(sf::Music * musik, int er);
        void testPosition() ;
        void mangePomme() ;

};

#endif // TESTREGRESSION_H

