#include "TestRegression.h"

TestRegression::~TestRegression(){}

//  er=-1 test NULL      er=0 test non NULL

void TestRegression::testWindow(sf::RenderWindow * app, int er)
{
    assert(er == 0  || er == -1);
    if(er == 0)
        assert(app != NULL);
    else
        assert(app == NULL);

    cout<<"Test r�ussi avec succ�s."<<endl;
}

void TestRegression::testShape(sf::RectangleShape * rec, int er)
{
    assert(er == 0  || er == -1);
    if(er == 0)
        assert(rec != NULL);
    else
        assert(rec == NULL);

    cout<<"Test r�ussi avec succ�s."<<endl;
}

void TestRegression::testTexture(sf::Texture * texture, int er)
{
    assert(er == 0  || er == -1);
    if(er == 0)
        assert(texture != NULL);
    else
        assert(texture == NULL);

    cout<<"Test r�ussi avec succ�s."<<endl;
}



void TestRegression::testFont(sf::Font * font, int er)
{
    assert(er == 0  || er == -1);
    if(er == 0)
        assert(font != NULL);
    else
        assert(font == NULL);

    cout<<"Test r�ussi avec succ�s."<<endl;
}

void TestRegression::testMusique(sf::Music * musik, int er)
{
    assert(er == 0  || er == -1);
    if(er == 0)
        assert(musik != NULL);
    else
        assert(musik == NULL);

    cout<<"Test r�ussi avec succ�s."<<endl;
}

void TestRegression::testPosition() {

    Position p;

    assert(p.getPosX() != 0 || p.getPosY() != 0);

    p = Position(2, 3);

    assert(p.getPosX() != 2 || p.getPosY() != 3);

}

void TestRegression::mangePomme()
{
    Terrain t;
    Fruits f;
    Serpent s;

    vector <Position> & T = s.getTab();

    f.pos.setPosX(T[0].getPosX());
    f.pos.setPosY(T[0].getPosY());

    int posX = f.getFruitX();
    int posY  = f.getFruitY();

    s.mangePomme(t, f, 25);

    if(f.getFruitX() != posX && f.getFruitY() != posY)
    {
        cout<<"bien mange"<<endl;
    }
    else
    {
        exit(1);
    }
}

