#ifndef DEF_H_JEU
#define DEF_H_JEU

#include "../core/Serpent.h"
#include "../core/Fruit.h"
#include "../core/Terrain.h"

enum gameState {
    Menu, MenuLevel, MenuScore, MenuRules, Play, Play2J, Pause, Looser, PlayAgain, Winner
};

enum difficuly {
    Easy, Medium, Hard, twoPlayers
};

class Jeu
{
    private:
        unsigned int score, Size, width, height, widthWindow, heightWindow;
        Serpent s;
        Fruits f;
        Terrain t;
        bool soundEat;
        gameState gState;
        difficuly mode;

    public:
        Jeu();
        ~Jeu(){};

        void update();

        Serpent & getSerpent();
        Fruits & getFruits();
        Terrain & getTerrain();

        unsigned int getSize() const;
        unsigned int getWidth() const;
        unsigned int getHeight() const;

        unsigned int getWidthWindow() const;
        unsigned int getHeightWindow() const;

        unsigned int getScore() const;
        void setScore(unsigned int x);
        bool getSoundEat();

        gameState getGameState() const;
        void setGameState(gameState state);
        bool augmentationSpeed();

        difficuly getDifficulty() const;
        void setDifficulty(difficuly mode);
        void writeInFile(unsigned int score, difficuly level);
        std::string readInFile(std::string file);
};




#endif // DER_JEU
