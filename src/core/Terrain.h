#ifndef DEF_TERRAIN
#define DEF_TERRAIN

#include <vector>

#include "Position.h"

enum Step {
    hG, hD, bG, bD
};

enum fireWall {
    corner1, corner2, corner3, corner4
};

class Terrain
{
    private:

        std::vector <Position> posWall;
        std::vector <Position> posStep;

    public:

        Terrain();
        ~Terrain(){};

        std::vector<Position> & getTabWall();
        std::vector<Position> & getTabStep();

        void setTabStep(int tabStart, Position posStart, Step sS);
        void setTabWall(int tabStart, Position posStart, fireWall sW);

};

#endif


