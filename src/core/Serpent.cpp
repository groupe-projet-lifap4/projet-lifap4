#include <iostream>
#include <stdlib.h>
//#include <conio.h>
#include <SFML/Audio.hpp>

#include "Serpent.h"
#include "../sfml/Draw.h"

using namespace std;
using namespace sf;


Serpent::Serpent()
{
    posSnake = vector<Position>(3);
    posSnake[0] = Position(2, 0);
    /*posSnake[1].setPosX(0);
    posSnake[1].setPosY(0);*/

    direction = DROITE;
   // vivant;
}


Direction Serpent::getDirection()
{
    return direction;
}


vector <Position> & Serpent::getTab()
{
    return posSnake;
}

//Réinitialisation des paramétres pour une nouvelle partie
void Serpent::setTab(int taille, int posX, int posY)
{
    posSnake.resize(taille);
    posSnake[0].setPosX(posX);
    posSnake[0].setPosY(posY);
}

//La tete est la case (0,0) le corps suit les mêmes Position que la tête
void Serpent::suivreTete()
{
    for(int i = posSnake.size() ; i > 0 ; --i)
        posSnake[i] = posSnake[i-1];
}

//Evolution des positions du serpent celon les déplacements (haut, bas, etc...)
void Serpent::posSuivante()
{
    int pX = posSnake[0].getPosX(), pY = posSnake[0].getPosY();

    if(direction == HAUT) posSnake[0].setPosY(pY-1);
    if(direction == BAS) posSnake[0].setPosY(pY+1);
    if(direction == GAUCHE) posSnake[0].setPosX(pX-1);
    if(direction == DROITE) posSnake[0].setPosX(pX+1);
}

//Pointeur this qui pointe vers lui même
void Serpent::setDirection(Direction direction)
{
    this->direction = direction;
}

/* Si la position de la tête & du fruit sont égauxon, on stocke la derniere position
du serpent & on la stocke dans un tableau */
bool Serpent::mangePomme(Terrain & te, Fruits & f, unsigned int Size)
{
    bool mange = false;
    if(posSnake[0].getPosX() == f.getFruitX() && posSnake[0].getPosY() == f.getFruitY())
    {
        unsigned int t = posSnake.size();
        posTailajouter.push_back(posSnake[t-1]);
        while(f.posTest(te.getTabStep()) || f.posTest(te.getTabWall()) || f.posTest(getTab()))
            f.posRand(Size);
        mange = true;
    }
    return mange;
}

/* On push la/les dernieres position du snake lorsqu'il mange une pomme & on les mets
dans le tableau de position du snake pour qu'elle soit mise à jour*/
void Serpent::ajouterTailSerpent()
{
    for(unsigned int i = 0 ; i < posTailajouter.size() ; i++) {
        posSnake.push_back(posTailajouter[i]);
    }
    posTailajouter.clear(); // clear car besoin temporairement
}

/* Avec cette fonction le serpent peut traverser un mur en sortant de l'autre côté */
void Serpent::traverseMur(unsigned int Size)
{
    if(posSnake[0].getPosX() < 0) posSnake[0].setPosX(Size);
    else if(posSnake[0].getPosX() > Size) posSnake[0].setPosX(0);
    else if(posSnake[0].getPosY() < 0) posSnake[0].setPosY(Size);
    else if(posSnake[0].getPosY() > Size) posSnake[0].setPosY(0);

}

/* Si le serpent touche un mur de la map il perd */
bool Serpent::wallDead(unsigned int Size) const
{
    return (posSnake[0].getPosX() < 0 || posSnake[0].getPosX() > Size ||
       posSnake[0].getPosY() < 0 || posSnake[0].getPosY() > Size);

}

/* Fonction booléen qui prend un paramètre un vector & compare la tête du serpent avec les
position contenu du tableau, dans le jeu cette fonction nous sert a savoir si le serpent est
entré en colision avec un obstacle de la map */
bool Serpent::deathColision(vector <Position> tab) const
{
    for(unsigned int i = 1 ; i < tab.size() ; i++)
        if(posSnake[0] == tab[i])
            return true;

    return false;
}






